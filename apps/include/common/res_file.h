#ifndef _RES_FILE_H_
#define	_RES_FILE_H_

#define RES_BT_CFG             	"bt_cfg.bin"
#define RES_HW_EQ_CFG			"eq_cfg.bin"
#define RES_SW_EQ_CFG         	"eq_cfg.bin"

#define RES_OID_MP3             "/system/oid.***"



#define RES_BT_MP3              "/system/bt_mode.***"
#define RES_CONNECT_MP3         "/system/bt_conn.***"
#define RES_DISCONNECT_MP3      "/system/bt_disc.***"
#define RES_PARING_MP3          "/system/bt_pair.***"



#define RES_MUSIC_MP3           "/system/music.***"

#define RES_REC_MP3             "/system/recmode.***"
#define RES_REC_PLAY_MP3        "/system/recplay.***"
#define RES_REC_START_MP3       "/system/recstart.***"
#define RES_REC_END_MP3         "/system/recstop.***"


#define RES_PC_MP3              "/pc.***"

#define RES_RADIO_MP3           "/radio.***"

#define RES_LINEIN_MP3          "/linein.***"

#define RES_RTC_MP3             "/rtc.***"

#define RES_ECHO_MP3            "/echo.***"





#define RES_RDFBOOK_MP3         "/system/rf_book.***"


#define RES_RING_MP3            "/ring.***"
#define RES_0_MP3               "/0.***"
#define RES_1_MP3               "/1.***"
#define RES_2_MP3               "/2.***"
#define RES_3_MP3               "/3.***"
#define RES_4_MP3               "/4.***"
#define RES_5_MP3               "/5.***"
#define RES_6_MP3               "/6.***"
#define RES_7_MP3               "/7.***"
#define RES_8_MP3               "/8.***"
#define RES_9_MP3               "/9.***"


#define RES_SD_ERROR_MP3        "/sd_error.***"

#define RES_POWER_ON_MP3        "/system/pwr_on.***"
#define RES_POWER_OFF_MP3       "/system/pwr_off.***"
#define RES_LOW_POWER_MP3       "/low_power.***"
#define RES_WARNING_MP3     	"/warning.***"
#define RES_TEST_MP3       		"/test.***"
#define RES_AYHERE_AMR       	"/ayhere.***"

#endif
