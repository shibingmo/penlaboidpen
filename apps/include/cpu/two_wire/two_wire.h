#ifndef __TWO_WIRE_H__
#define __TWO_WIRE_H__
#include "sdk_cfg.h"

    #if OID_PEN_EN
    void master_setup_slave_init();
    u32 get_oidcode(void);
    u8 get_readflag(void);
    #endif // OID_PEN_EN

#endif // __TWO_WIRE_H__
