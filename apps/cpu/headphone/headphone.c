#include "headphone.h"
#include "sdk_cfg.h"
#include "common.h"
#include "dac.h"
#include "task_manager.h"
#include "dev_manage.h"

#define HEADPHONE_DBG
#ifdef  HEADPHONE_DBG

#define headphone_printf         log_printf

#else
#define headphone_printf(...)
#endif    //HEADPHONE_DBG

void headphone_inout_detect(void)
{
    static  u8 headphone_in_time = 0;
    static  u8 headphone_out_time = 0;

        CHECK_PIN_IN();

        if( (!CHECK_PIN_STATE()) && (headphone_in_time != 5) )
        {
            headphone_in_time++;
            headphone_out_time = 0;
            headphone_printf("IN %d, %d...\n", headphone_in_time, headphone_out_time);
            if(headphone_in_time == 4)//1s
            {
                SPEAKER_DECODER_LOW();

                headphone_in_time = 5;
            }
        }
        else if( (CHECK_PIN_STATE()) && (headphone_out_time != 5) )
        {
            headphone_out_time++;
            headphone_in_time = 0;
            headphone_printf("out %d, %d...\n", headphone_in_time, headphone_out_time);
            if(headphone_out_time == 4)//1s
            {
                extern DEV_HANDLE get_emitter_dev(void);
                if((task_get_cur() == TASK_ID_BT) && (get_emitter_dev() != NULL))
                {
                    SPEAKER_DECODER_LOW();
                }
                else
                {
                    SPEAKER_DECODER_HIGH();
                }


                headphone_out_time = 5;
            }
        }

    return ;
}


LOOP_DETECT_REGISTER(hp_detect) = {
    .time = 125,
    .fun  = headphone_inout_detect,
};
