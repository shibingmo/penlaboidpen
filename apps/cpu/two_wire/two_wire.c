#include "uart.h"
#include "cpu/two_wire/two_wire.h"
#include "sdk_cfg.h"
#include "typedef.h"
#include "common/common.h"
#include "task_manager.h"
#include "led.h"

/*

master ---- DSP/MCU
slave ----- OID decoder

*/

/*
Default : SCK is kept low by Master, and SDIO is released and pulled high by
external pull-up resister.
*/

#if OID_PEN_EN

#define tw_delay()      delay(100)//7.6us

#define TW_PORT			JL_PORTA

#define TW_SCK				2
#define TW_SDIO				3

#define LOW                 0
#define HIGH                1

#define tw_clk_out()    do{TW_PORT->DIR &= ~BIT(TW_SCK);TW_PORT->PU &= ~BIT(TW_SCK);}while(0)
#define tw_clk_in()     do{TW_PORT->DIR |=  BIT(TW_SCK);TW_PORT->PU |=  BIT(TW_SCK);TW_PORT->PD &= ~BIT(TW_SCK);}while(0)
#define tw_clk_h()      do{TW_PORT->OUT |=  BIT(TW_SCK);TW_PORT->DIR &=~BIT(TW_SCK);}while(0)
#define tw_clk_l()      do{TW_PORT->OUT &= ~BIT(TW_SCK);TW_PORT->DIR &=~BIT(TW_SCK);}while(0)


#define tw_data_out()   do{TW_PORT->DIR &= ~BIT(TW_SDIO);TW_PORT->PU &= ~BIT(TW_SDIO);}while(0)
#define tw_data_in()    do{TW_PORT->DIR |=  BIT(TW_SDIO);TW_PORT->PU |=  BIT(TW_SDIO);TW_PORT->PD &= ~BIT(TW_SDIO);}while(0)//
#define tw_data_r()     (TW_PORT->IN&BIT(TW_SDIO))
#define tw_data_h()     do{TW_PORT->OUT |=  BIT(TW_SDIO);TW_PORT->DIR &= ~BIT(TW_SDIO);}while(0)
#define tw_data_l()     do{TW_PORT->OUT &= ~BIT(TW_SDIO);TW_PORT->DIR &= ~BIT(TW_SDIO);}while(0)



#define DEBOUNCE_TIMES 3

TASK_ID_TYPE get_emitter_task_id(void);
static u32 oidcode = 0xFFFFFFFF;
static u32 last_oidcode = 0xFFFFFFFF;
u8 timer_enable = 0;
void clear_oidcode(void);
extern u32 os_time_get(void);
static void master_init()
{
    tw_clk_out();
    tw_clk_l();

    tw_data_in(); //ready to rec data
    tw_delay();
}


/*
Master initiates a transfer cycle by changing SCK from low to high.
*/
static void master_send_start()
{
    tw_clk_l();
    tw_delay();
    tw_clk_h();
    tw_delay();
}


/*
If Master keeps low on SCK over 78¦Ìs.
Slave supposes that the transfer cycle is finished.
*/
static void master_send_end()
{
    tw_clk_out();
    tw_data_in();//release sdio
    tw_clk_l();
    delay(1200);
}


/*
Before Read cycle, Slave generates transfer request (pulling low SDIO line) to inform
Master.
*/
static bool master_check_revstate()
{
    tw_data_in();
    tw_clk_l();

    delay(1000);
    if(!tw_data_r())//data low
    {
        return TRUE;
    }
    else
        return FALSE;
}


static u16 master_wait_ack()
{
	u16 i = 0, receive_dat = 0, count = 0;

    if(master_check_revstate() == FALSE)//no ACK
        return receive_dat;

    master_send_start();

    tw_data_out(); // read control bit
    tw_delay();
    tw_data_l();
    tw_delay();

    tw_clk_l();    //positive edge
    tw_delay();
    tw_clk_h();
    tw_delay();

    tw_data_in();
    tw_delay();

	for(i=0; i<16; i++)
	{
	    tw_clk_h();
        tw_delay();

        tw_clk_l();
        tw_delay();

        receive_dat <<= 1;
        if(tw_data_r())
            receive_dat |= 0x01;


	}

    master_send_end();

//    log_printf("wait ACK : 0x%x\n", receive_dat);
	return receive_dat;
}



static void master_send_onebit(bit1 bit)
{
    tw_data_out();
    tw_delay();

    if(bit)
        tw_data_h();
    else
        tw_data_l();

    tw_clk_h();
    tw_delay();
    tw_clk_l();
    tw_delay();

}


static void master_send_onebyte(u8 data)
{
    log_printf("master_send_one byte start:0x%02x\n", data);

    u8 temp = data, count = 0;

    while(count++ <10)//SDIO should be checked before every Write cycle.
    {
        if(master_check_revstate() == FALSE)
            break;
        else
        {
            master_send_end(); //end signal
            log_printf("stop read.\n");
        }
    }

    if(count==10) //not write.
        return;


    master_send_start(); //start signal

    master_send_onebit(1);//write control bit 1

    for(int i=0; i<8; i++)                //write one byte
    {
        if((data & 0x80)>0)               //MSB bit
           master_send_onebit(1);         //pull high data IO
        else
           master_send_onebit(0);         //pull low data IO

        data <<= 1;
    }

    master_send_end();                    //end signal

    count = 0;
    while(count++ < 200)
    {
        if(master_wait_ack() == ((u16)temp+1) )    //check send is valid.
        {
            log_printf("send valid.\n");
            break;
        }
        else
        {
            log_printf("send invalid, count: %d\n", count);
            tw_delay();
        }
    }

    log_printf("master_send_one byte  end.............\n");
    return  ;
}

/*
Multi-Write Command
*/
static void master_send_nbyte(u8 *cmd, u8 len)
{
    if(!cmd || len == 0)
        return ;

    while (len--)
    {
        master_send_onebyte(*cmd++);
        delay_n10ms(30);//between two commands must > 250ms
    }

    return ;
}


static u8 master_readfrom_slave_onebyte()
{
    u8 i, receive_dat = 0;

 //   tw_clk_l();
 //   tw_delay();

	for(i=0; i<8; i++)
	{


        tw_clk_h();
        tw_delay();

	    tw_clk_l();
        tw_delay();

        if(tw_data_r())
            receive_dat |= (1<<(7-i));

	}


	//log_printf("receive_dat:0x%02x\n", receive_dat);
    return receive_dat;
}


static void master_readfrom_slave_nbyte(u8 data[], u8 n)
{
    memset(data, 0, n);
    master_send_start(); //start signal

    tw_data_out(); // read control bit  SCK:H
    tw_data_l();
    tw_delay();

    tw_clk_l();    //positive edge
    tw_delay();

    tw_data_in();  //released SDIO
    tw_delay();

    while(n--)
    {
       *data = master_readfrom_slave_onebyte();

        data++;
    }

    master_send_end();

    return ;
}

u8 reading_flag = 0;

static bool master_readfrom_slave(u8 *data)
{
    static u8 packet_same_times;
    static u8 data_temp1[4];
    static u8 data_temp2[4];
    static u8 data_temp3[4];
    static u8 data_temp4[4];
    static u8 interval_time = 0;
    static u8 led_time_cnt = 0;
        if(led_time_cnt)
        {
            led_time_cnt--;
            if((led_time_cnt==0)&&((task_get_cur() == TASK_ID_OID)||(get_emitter_task_id()==TASK_ID_OID)))
            {
                led_fre_set(C_BLED_ON_MODE);
            }
        }

        if((master_check_revstate()==TRUE))//check rev state
        {
           master_readfrom_slave_nbyte(data,8);
           interval_time = 25;
           //log_printf("recivdata: 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x\n",data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);
           if(!(data[0]&BIT(4)))//valid
           {  //  log_printf("recivdata: 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x\n",data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);
              //log_printf("data_temp: 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x\n",data_temp[0],data_temp[1],data_temp[2],data_temp[3],data_temp[4],data_temp[5],data_temp[6],data_temp[7]);





               if(packet_same_times ==1)
               {

                    if((0==memcmp(data_temp1, data_temp2, 4)) && (0!=memcmp(data_temp2, &data[4], 4))&& (0!=memcmp(data_temp3, &data[4], 4)))
                    {
                                                memcpy(data_temp3, &data[4], 4);
                    }
                    else if((0==memcmp(data_temp1, data_temp2, 4)) && (0==memcmp(data_temp2, &data[4], 4)))
                   {
                                                memset(data_temp3, 0x33, 4);
                                                memset(data_temp4, 0x44, 4);
                   }

                    else if((0==memcmp(data_temp3, &data[4], 4))&&(0!=memcmp(data_temp2, &data[4], 4))&& (0!=memcmp(data_temp3, data_temp4, 4)))
                    {
                                                memcpy(data_temp4, &data[4], 4);
                    }

                    else if((0==memcmp(data_temp3, data_temp4, 4)) && (0!=memcmp(data_temp4, &data[4], 4)))
                    {
                        memset(data_temp3, 0x33, 4);
                        memset(data_temp4, 0x44, 4);
                    }
                    else if((0==memcmp(data_temp3, data_temp4, 4)) && (0==memcmp(data_temp4, &data[4], 4)) && (0!=memcmp(data_temp2, data_temp4, 4)))
                    {
                        if(packet_same_times == 1)
                        {
                              packet_same_times =1;
                              led_time_cnt = 50;

                            memcpy(data_temp1, &data[4], 4);
                            memcpy(data_temp2, &data[4], 4);

                            memset(data_temp3, 0x33, 4);
                            memset(data_temp4, 0x44, 4);

                              return TRUE;
                        }
                    }
               }

               else
                if((0!=memcmp(data_temp1, &data[4], 4)) && (0!=memcmp(data_temp2, &data[4], 4))&& (0!=memcmp(data_temp2, data_temp1, 4)))
                {
                    memcpy(data_temp1,&data[4], 4);
                }
                else if((0==memcmp(data_temp1, &data[4], 4)) && (0!=memcmp(data_temp2, &data[4], 4))&&(0!=memcmp(data_temp2, data_temp1, 4)))
                {
                    memcpy(data_temp2,&data[4], 4);
                }
                else if((0==memcmp(data_temp1, data_temp2, 4)) && (0==memcmp(data_temp2, &data[4], 4)))
                {
                    if(packet_same_times == 0)
                    {
                          packet_same_times =1;
                          led_time_cnt = 50;
                          return TRUE;
                    }
                }

           }
            else
            {
                return FALSE;
            }
        }
        else
        {
            if(interval_time == 0)
            {
                memset(data_temp1, 0x11, 4);
                memset(data_temp2, 0x22, 4);
                memset(data_temp3, 0x33, 4);
                memset(data_temp4, 0x44, 4);

                packet_same_times = 0;
                reading_flag      = 0;
                clear_oidcode();
            }
            else
                interval_time--;

            return FALSE;
        }

        return FALSE;
}


static u32 master_parse_receive_data(u8 *data)
{
    return ((u32)data[4]<<24)|((u32)data[5]<<16)|((u32)data[6]<<8)|((u32)data[7]);
}

static u32 master_receive_end_parse_data(void)
{
    u8 data[8]={0};

    if(master_readfrom_slave(data))
    {
        return master_parse_receive_data(data);
    }

    return 0;
}

void set_oidcode(u32 code)
{
    if(code == 0)
        return ;


    oidcode =  code;


    if((task_get_cur() == TASK_ID_OID)||(get_emitter_task_id()==TASK_ID_OID))
    {
        led_fre_set(C_BLED_OFF_MODE);
    }

    extern void reset_poweroff_time(void);
    reset_poweroff_time();
    reading_flag = 1;

    log_printf("set_oidcode:0x%08X\n", oidcode);
    log_printf("inininin:%d\n",os_time_get()*10);
}

void clear_oidcode(void)
{
    oidcode = 0xFFFFFFFF;
}

u32 get_oidcode(void)
{
    u32 ret_oidcode;

    ret_oidcode = oidcode;
    oidcode = 0xFFFFFFFF;

    reading_flag = 0;

    //log_printf("get_oidcode :%d\n", ret_oidcode);
    return ret_oidcode;
}

u8 get_readflag(void)
{
    u32 ret_reading_flag;

    ret_reading_flag = reading_flag;
    reading_flag = 0;

    return ret_reading_flag;
}

// Setup decoder SOC from DSP/MCU
void master_setup_slave_init()
{
    u8 data[8] = {0}, count= 0;
    u8 init[2] = {0x27, 0x40};
    //exit configuration mode end enter to normal mode
    tw_clk_out();
    tw_clk_h();
    delay_n10ms(5);//over 20ms and less than 2sec

    master_init(); //Default state
    log_printf("\n\n\n\n\n\n\n---------------master_setup_slave_init------------------\n\n\n\n\n\n");


    while(count < 100)//check Decoder SOC is already entered to normal mode.
    {
        //log_printf("count:%d\n\n\n", count);
        if(master_check_revstate())//check rev state
        {
            memset(data, 0, sizeof(data));
            master_readfrom_slave_nbyte(data,8);
            log_printf("master_readfrom_slave_nbyte:0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x\n",data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);

            if(data[6]==0xFF && data[7]==0xF8)
                break;
            else if(data[6]==0xFF && data[7]==0xF6)//check oidpen head.
            {
                tw_data_in();
                tw_clk_in();
                while(1);
            }
        }
        delay_2ms(1);
        count++;
    }

    if(count == 100)
    {
        log_printf("\n\n\n\n\n\n\n---------------init two wire fail------------------\n\n\n\n\n\n");
    }
    else
    {
        log_printf("Decoder SOC has already entered to normal mode.\n\n\n");
        master_send_nbyte(init, sizeof(init));
    }

        timer_enable = 1;
}

void master_receive_oidcode(void)
{
    u8 data[8]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
    u32  code;
    if(timer_enable ==0)
    return ;


    if(master_readfrom_slave(data))
    {
        code = ((u32)data[4]<<24)|((u32)data[5]<<16)|((u32)data[6]<<8)|((u32)data[7]);
        set_oidcode(code);
    }



}

LOOP_DETECT_REGISTER(oidcode_detect) = {
    .time = 5,
    .fun  = master_receive_oidcode,
};

#endif
