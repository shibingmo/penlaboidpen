#include "irq_api.h"
#include "sdk_cfg.h"

void port_isr(void)
{
	JL_WAKEUP->CON2 |= BIT(2);
	printf("------port_isr-------\n");

	SWITCH_HIGH();

}

//IRQ_REGISTER(IRQ_PORT_IDX, port_isr);


void port_isr_init(void)
{
	printf("------port_isr_init-------\n");

	JL_PORTC->DIR |= BIT(3);
	JL_PORTC->DIE |= BIT(3);
	//JL_PORTC->PU |=  BIT(3);
	//JL_PORTC->PD &= ~BIT(3);
	JL_PORTC->PU &= ~BIT(3);
	JL_PORTC->PD |= BIT(3);

	JL_WAKEUP->CON0 |= BIT(2);//检查PA8电平触发
	//JL_WAKEUP->CON1 |= BIT(2);//下降沿触发
	 JL_WAKEUP->CON1 &= ~BIT(2);//上升沿触发

    JL_IOMAP->CON2 &= ~0x1F;
    JL_IOMAP->CON2 |= BIT(3);
    JL_IOMAP->CON2 |= BIT(4);

	JL_WAKEUP->CON2 |= BIT(2);

//    IRQ_REQUEST(IRQ_PORT_IDX, port_isr);
}

void port_isr_exit(void)
{
    irq_handler_unregister(IRQ_PORT_IDX);
}
