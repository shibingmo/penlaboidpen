#include "task_oidmp3.h"
#include "task_oidmp3_key.h"
#include "msg.h"
#include "string.h"
#include "uart.h"
#include "audio/dac_api.h"
#include "dac.h"
#include "audio/audio.h"
#include "dev_manage.h"
#include "dev_mg_api.h"
#include "music_player.h"
#include "task_common.h"
#include "warning_tone.h"
#include "fat_io.h"
#include "fs_io.h"
#include "two_wire.h"
#include "clock.h"
#include "sdk_cfg.h"
#include "led.h"
#include "updata.h"
#include "dev_manage.h"
#if 1//OID_PEN_EN

#define OIDMP3_TASK_DEBUG_ENABLE
#ifdef OIDMP3_TASK_DEBUG_ENABLE
#define oidmp3_task_printf log_printf
#else
#define oidmp3_task_printf(...)
#endif

extern u32 os_time_get(void);
static u32 time_test = 0;
static MUSIC_DECODER_ST music_status1;
static DEV_HANDLE cur_use_dev1= NULL;
static u8 oidmp3_cur_open_book = 0;
 char oidmp3_playfile_path1[100]={'\0'};
 char oidmp3_playfile_path2[100]={'\0'};

 char bookcode[10];
 char foldercode[10];
u32 last_code = 0xFFFFFFFF;
u32 last_foldercode = 0xFFFFFFFF;

#define PASS_VOICE_TONE      ((void *)1)


static void oidmp3_play_mutex_init(void *priv)
{
    oidmp3_task_printf("%s in...\n", __func__);
}

static void oidmp3_play_mutex_stop(void *priv)
{
    oidmp3_task_printf("%s in..., priv:0x%x...\n", __func__, priv);

    MUSIC_PLAYER *obj = priv;
    music_status1 = music_player_get_status(obj);
    cur_use_dev1 = music_player_get_cur_dev(obj);

    if (obj && cur_use_dev1)
    {
        music_player_destroy(&obj);
    }
    oidmp3_task_printf("%s in..., priv:0x%x...\n", __func__, priv);
}

static MUSIC_PLAYER *oidmp3_play_start(void)
{
	oidmp3_task_printf("%s in...\n", __func__);

	if(is_cur_resource("oidmp3"))
    {
        mutex_resource_release("tone");
        mutex_resource_release("oidmp3");
    }


    MUSIC_PLAYER *obj = NULL;
    obj = music_player_creat();
    music_status1 = MUSIC_DECODER_ST_PLAY;

    oidmp3_task_printf("music_player_creat:0x%x\n", obj);
    if (obj) {
        mutex_resource_apply("oidmp3", 3, oidmp3_play_mutex_init, oidmp3_play_mutex_stop, obj);
    }
    return obj;
}

static void oidmp3_play_stop(void *priv)
{
	oidmp3_task_printf("%s in...\n", __func__);


    task_common_msg_deal(priv, NO_MSG);
}

static void oidmp3_msg_filter(int *msg)
{
    switch (*msg)
    {
	    case MSG_MUSIC_PP:
	    case MSG_MUSIC_NEXT_FILE:
	    case MSG_MUSIC_PREV_FILE:
	    case MSG_MUSIC_FF:
	    case MSG_MUSIC_FR:
	    case MSG_MUSIC_AB_RPT:
	    case MSG_HALF_SECOND:
	        *msg = NO_MSG;
	        break;
    }
}


tbool player_is_exist_playfile(FILE_OPERATE *fop_api, u8 *path)
{
    u32 ret;

    if (fop_api == NULL)
    {
        puts("fop_api == NULL\n");
        return false ;
    }


    file_operate_dev_bp(fop_api, NULL);
    file_operate_set_path(fop_api, path, 0);

    ret = fs_get_file_bypath(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), (u8 *)fop_api->fop_info->filepath);
    if(!ret)
    {
        puts("player_is_exist_playfile: exist play file.\n");
        fs_close(&(fop_api->fop_file->file_hdl));
        return true;
    }
    else
    {
        puts("player_is_exist_playfile: no file.\n");
        return false;
    }
}





static u32 oidmp3_polling_to_get_oidcode(void)
{
	return get_oidcode();//接收-解析笔头数据
}

u8 is_double_play_error = 0;
u8 folder_exist = 0;
static bool oidmp3_check_oidcode(FILE_OPERATE *fop_api, u32 oidcode)
{
	u8 bkcode = 0, fdcode = 0;
    char temp[50] = {'\0'};

	if(oidcode == 0xFFFFFFFF){
        return FALSE;
	}

	if(oidcode>= 65001 && oidcode <= 65099) // select book
	{
	    memset(oidmp3_playfile_path1, '\0', sizeof(oidmp3_playfile_path1));
	    memset(oidmp3_playfile_path2, '\0', sizeof(oidmp3_playfile_path2));
	    last_code = 0xFFFFFFFF;
	    oidmp3_cur_open_book = 0;
        is_double_play_error =0;
		bkcode = oidcode % 65000 ;
        oidmp3_cur_open_book = bkcode;

		sprintf(bookcode, "B%02d", bkcode);	//make sure bookcode
		sprintf(oidmp3_playfile_path1, "/BOOK/%s/title.mp3", bookcode);
        task_post_msg(NULL, 1, MSG_MUSIC_PLAY1);
	}
	else if(oidmp3_cur_open_book != 0)
	{
		fdcode = oidcode/1000;	//foldercode

		sprintf(foldercode, "%03d", fdcode);

		if(last_code == oidcode)
        {
            if(fdcode != last_foldercode)
            {
                last_foldercode = fdcode;

                memset(temp, '\0', sizeof(temp));
                sprintf(temp, "/BOOK/%s/%sa/", bookcode, foldercode);

                if(player_is_exist_playfile(fop_api, (unsigned char*)temp))
                {
                    folder_exist = 1;
                }
                else
                {
                    folder_exist = 0;
                }
            }

            if(fdcode == 0)
                sprintf(oidmp3_playfile_path2, "/BOOK/%s/%sa/%d.mp3", bookcode, foldercode, oidcode);
            else
                sprintf(oidmp3_playfile_path2, "/BOOK/%s/%sa/%06d.mp3", bookcode, foldercode, oidcode);


            if(folder_exist)
            {
                if(is_double_play_error ==0)
                {
                    task_post_msg(NULL, 1, MSG_MUSIC_PLAY2);
                }
                else
                {
                    task_post_msg(NULL, 1, MSG_MUSIC_PLAY1);
                }
            }
            else
            {
                task_post_msg(NULL, 1, MSG_MUSIC_PLAY1);
            }

        }
        else
        {
            is_double_play_error =0;

            last_code = oidcode;
            memset(oidmp3_playfile_path1, '\0', sizeof(oidmp3_playfile_path1));
            memset(oidmp3_playfile_path2, '\0', sizeof(oidmp3_playfile_path2));

            if(fdcode == 0)
            {
                sprintf(oidmp3_playfile_path1, "/BOOK/%s/%s/%d.mp3", bookcode, foldercode, oidcode);
            }
            else
            {
                sprintf(oidmp3_playfile_path1, "/BOOK/%s/%s/%06d.mp3", bookcode, foldercode, oidcode);
            }

            task_post_msg(NULL, 1, MSG_MUSIC_PLAY1);
        }

	}

	return TRUE;
}


void oidmp3_play_file(MUSIC_PLAYER *obj, int msg)
{
	//if(oidmp3_cur_open_book == 0)
	//{
		//tone_play(TONE_RFBOOK, 0);
	//	return;
	//}
	//else

	tbool ret = true;

	{
        if (obj == NULL)
        {
            oidmp3_task_printf("fun = %s in, line = %d, obj == NULL\n", __func__, __LINE__);
           // return ;
           obj = oidmp3_play_start();


        }

        if ((obj->dop == NULL) || (obj->fop == NULL))
        {
            oidmp3_task_printf("fun = %s in, line = %d, obj->fop == NULL\n", __func__, __LINE__);
            obj = oidmp3_play_start();
        }
	}


	if(msg == MSG_MUSIC_PLAY1)
    {
        ret = music_player_play_path_file(obj, (unsigned char *)oidmp3_playfile_path1, 0);
    }
    else if(msg == MSG_MUSIC_PLAY2)
    {
        oidmp3_task_printf("start play2:%d\n",os_time_get()*10);
        ret = music_player_play_path_file(obj, (unsigned char *)oidmp3_playfile_path2, 0);

        if(ret == false)
        {
            oidmp3_task_printf("start play1:%d\n",os_time_get()*10);
            is_double_play_error =1;
            ret = music_player_play_path_file(obj, (unsigned char *)oidmp3_playfile_path1, 0);
        }
    }

    oidmp3_task_printf("ret = %d \n", ret);
}


tbool turnto_oidmode_check(void)//检测是否跳转到OID
{
    if(get_readflag())
    {
        log_printf("TURN to----OID MODE\n");
        task_switch(TASK_ID_OID, PASS_VOICE_TONE);//跳过模式提示音
        return TRUE;
    }


    return FALSE;
}



static tbool task_oidmp3_skip_check(void **priv)
{
	oidmp3_task_printf("%s in...\n", __func__);

    u32 dev_status;

    oidmp3_task_printf("cur_use_dev1:0x%x,0x%x\n", cur_use_dev1, *priv);

    //check some device online
    if (*priv == NULL || *priv != PASS_VOICE_TONE)
    {
        if (cur_use_dev1 != NULL)
        {
            if (!dev_get_online_status(cur_use_dev1, &dev_status))
            {
                if (dev_status != DEV_ONLINE)
                {                               //上一次退出时的设备不在线，重新获取一个
                    *priv = dev_get_fisrt(MUSIC_DEV_TYPE, DEV_ONLINE);
                }
                else
                 {                                                      //在线，使用上次保存的设备播放
                    *priv = cur_use_dev1;
                }
            }
        }
        else
        {
            *priv = dev_get_fisrt(MUSIC_DEV_TYPE, DEV_ONLINE);
        }

        printf("*priv:0x%x\n", *priv);
        if (*priv == sd0) {
            puts("[PRIV SD0]\n");
        } else if (*priv == sd1) {
            puts("[PRIV SD1]\n");
        } else if (*priv == usb) {
            puts("[PRIV USB]\n");
        } else {
            puts("[PRIV CACHE]\n");
        }

        if (*priv == NULL)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    return true;
}


static void *task_oidmp3_init(void *priv)
{
	oidmp3_task_printf("%s in...priv:0x%x, 0x%x\n", __func__,priv, PASS_VOICE_TONE);
    MUSIC_PLAYER *obj = NULL;

    fat_mem_init(NULL, 0);  //使用默认的RAM， overlay task

    obj = oidmp3_play_start();

    dac_channel_on(MUSIC_CHANNEL, 0);
    //set_sys_vol(sound.vol.sys_vol_l, sound.vol.sys_vol_r, FADE_ON);
    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT_MUSIC_TASK);

    if(priv != PASS_VOICE_TONE)
    {
        oidmp3_check_oidcode(obj->fop, 65001);
    }


    led_fre_set(C_BLED_ON_MODE);
	oidmp3_task_printf("%s in  %x.\n", __func__, obj->fop->fop_info->cur_sel_mode);
    return obj;
}

static void task_oidmp3_exit(void **hdl)
{
    oidmp3_task_printf("task_oidmp3_exit !!\n");
    /* dac_channel_off(MUSIC_CHANNEL, 0); */
    //cur_use_dev1 = music_player_get_cur_dev(*hdl);
    music_player_destroy((MUSIC_PLAYER **)hdl);

    mutex_resource_release("oidmp3");

    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT);
    set_sys_freq(SYS_Hz);
    task_clear_all_message();

    get_readflag();//clean the last flag.
    last_code = 0xFFFFFFFF;
    puts("oidmp3_exit ok!\n");
}


u8 test =0;
static void task_oidmp3_deal(void *hdl)
{
    oidmp3_task_printf("task_oidmp3_deal ~~~~~~~~~~~~\n");

    int msg = NO_MSG;
    u32 err;
    u8 music_start = 0;
    tbool ret = true;
    MUSIC_PLAYER *obj = (MUSIC_PLAYER *)hdl;

    if (obj == NULL) {
        oidmp3_task_printf("fun = %s, line = %d, obj == NULL...\n", __func__, __LINE__);
        ret = false;
    }

    while (1)
     {
        task_get_msg(0, 1, &msg);

        if (get_tone_status()) {    //提示音还未播完前过滤涉及解码器操作的消息
            oidmp3_msg_filter(&msg);
        }

        if (task_common_msg_deal(obj, msg) == false) {
            if (music_player_get_cur_dev(obj) != (void *)cache) {
                cur_use_dev1 = music_player_get_cur_dev(obj);
            }
            oidmp3_task_printf("exit oidmp3_task\n");
            oidmp3_play_stop(obj);
            return;
        }

        oidmp3_check_oidcode(obj->fop, oidmp3_polling_to_get_oidcode());

        if (NO_MSG == msg) {
            continue;
        }


        switch (msg) {
        case MSG_ONE_SECOND:
         if(USB_IN_STATE() == 1)
        {
            oidmp3_task_printf("\n---------------MSG_ONE_SECOND\n");

            //extern void set_dev_start(void);
            //set_dev_start();
            //SWITCH_HIGH();
        }
            break;
        case MSG_MUSIC_PLAY1:
            oidmp3_play_file(obj, msg);

            break;
        case MSG_MUSIC_PLAY2:
            oidmp3_play_file(obj, msg);

            break;
        case MSG_VOL_UP:
            oidmp3_task_printf("MSG_VOL_UP:%d\n", music_player_get_status(obj));
            if(music_player_get_status(obj)== MUSIC_DECODER_ST_PLAY)
                break;

            if(sound.vol.sys_vol_l == get_max_sys_vol(0))
                tone_play(TONE_VOL_MAX, 0);
            else
                tone_play(TONE_VOL_UP, 0);
            break;
        case MSG_VOL_DOWN:
            oidmp3_task_printf("MSG_VOL_DOWN:%d\n", music_player_get_status(obj));
            if(music_player_get_status(obj)== MUSIC_DECODER_ST_PLAY)
                break;

            if(get_sys_vol(0)==10)
                tone_play(TONE_VOL_MIN, 0);
            else
                tone_play(TONE_VOL_DOWN, 0);
            break;

        case SYS_EVENT_DEC_FR_END:
        case SYS_EVENT_DEC_FF_END:
        case SYS_EVENT_DEC_END:

            music_decoder_stop(obj->dop);
            oidmp3_task_printf("SYS_EVENT_DEC_END, fun = %s, line = %d\n", __func__, __LINE__);

            break;

        case SYS_EVENT_DEC_DEVICE_ERR:
            puts("SYS_EVENT_DEC_DEVICE_ERR\n");

            break;

        case SYS_EVENT_PLAY_SEL_END:
            tone_var.status = 0;
            tone_var.idx    = 0;

            oidmp3_task_printf("SYS_EVENT_PLAY_TONE_END\n");
            if(obj->dop==NULL)
               oidmp3_task_printf("obj->dop==NULL , line = %d\n", __LINE__);

            if(obj->fop==NULL)
               oidmp3_task_printf("obj->dop==NULL , line = %d\n", __LINE__);



            break;

        case MSG_MUSIC_AB_RPT:
            oidmp3_task_printf("MSG_MUSIC_AB_RPT\n");
            music_player_ab_repeat_switch(obj);
            break;

        case MSG_MUSIC_U_SD:
            break;
        case MSG_SD0_MOUNT_SUCC:
            break;
        case MSG_SD1_MOUNT_SUCC:
            break;
        case MSG_USB_MOUNT_SUCC:
            break;
        case MSG_SD0_OFFLINE:
            if (sd0 == music_player_get_cur_dev(obj) && MUSIC_PLAYRR_ST_PAUSE == music_player_get_status(obj)) {  //防止拔出设备时重复推消息
                task_post_msg(NULL, 1, SYS_EVENT_DEC_DEVICE_ERR);
            }
            break;
        case MSG_SD1_OFFLINE:
            if (sd1 == music_player_get_cur_dev(obj) && MUSIC_PLAYRR_ST_PAUSE == music_player_get_status(obj)) {
                task_post_msg(NULL, 1, SYS_EVENT_DEC_DEVICE_ERR);
            }
            break;
        case MSG_HUSB_OFFLINE:
            break;
        case MSG_MUSIC_PP:
            break;
        case MSG_INPUT_NUMBER_END:
        case MSG_INPUT_TIMEOUT:
            break;
        case MSG_MUSIC_SPC_FILE:
            break;
        case MSG_MUSIC_NEXT_FILE:
            break;
        case MSG_MUSIC_PREV_FILE:
            break;
        case MSG_MUSIC_PREV_FOLDER:
            break;
        case MSG_MUSIC_NEXT_FOLDER:
            break;
        case MSG_MUSIC_RPT:
            break;
        case MSG_MUSIC_FF:
            break;
        case MSG_MUSIC_FR:
            break;
        case MSG_MUSIC_FF_KEY_UP:
            break;
        case MSG_MUSIC_FR_KEY_UP:
            break;
        case MSG_MUSIC_FFR_DONE:
            break;
        case MSG_MUSIC_DEL_FILE:
            break;
        case MSG_UPDATA:
            log_printf("MSG_UPDATA:%x\n", file_operate_get_dev(obj->fop));
            device_updata(sd0);
            break;
        case MSG_HALF_SECOND:
		//oidmp3_task_printf("msg = %d\n", msg);
            break;

        default:
            break;
        }

        if (ret == false && obj != NULL) {
            oidmp3_task_printf("music player play err!!, fun = %s, line = %d\n", __func__, __LINE__);
            oidmp3_play_stop(obj);
            task_switch(TASK_ID_TYPE_NEXT, NULL);
            return ;
        }
    }
}


#if BT_EMITTER_EN

void emitter_oidmp3_thread(FILE_OPERATE *fop_api)
{
    oidmp3_check_oidcode(fop_api, oidmp3_polling_to_get_oidcode());
}

static u8 emitter_oidmp3_flag = 0;
void set_emitter_oidmp3(u8 flag)
{
    emitter_oidmp3_flag = flag;
}

u8 is_emitter_oidmp3(void)
{
    return  emitter_oidmp3_flag;
}

void *emitter_oidmp3_mode_start(void *dev)
{
    static DEV_HANDLE cur_use_dev = NULL;
    MUSIC_PLAYER *obj = NULL;
    tbool ret = 0;

    set_emitter_oidmp3(1);

    obj = music_player_creat();

    oidmp3_check_oidcode(obj->fop, 65001); //play title

    cur_use_dev = dev;

    if ((obj == NULL) || (cur_use_dev == NULL)) {
        printf("emitter_oidmp3_play err0:%x,%x\n", obj, cur_use_dev);
        return NULL;
    }
    return obj;
}
void emitter_oidmp3_mode_stop()//like as emitter_music_stop()
{
    set_emitter_oidmp3(0);
    last_code = 0xFFFFFFFF;
}

#endif







const TASK_APP task_oidmp3_info = {
    .skip_check = NULL,//task_oidmp3_skip_check,//
    .init 		= task_oidmp3_init,
    .exit 		= task_oidmp3_exit,
    .task 		= task_oidmp3_deal,
    .key 		= &task_oidmp3_key,
};

#endif // OID_PEN_EN
