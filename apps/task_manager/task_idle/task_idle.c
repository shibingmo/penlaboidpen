#include "task_idle.h"
#include "task_idle_key.h"
#include "msg.h"
#include "task_manager.h"
#include "task_common.h"
#include "audio/dac_api.h"
#include "dac.h"
#include "power_manage_api.h"
#include "dev_manage.h"
#include "warning_tone.h"
#include "led.h"
#include "file_operate.h"
#include "fat_io.h"
#include "common.h"

#ifdef SUPPORT_MS_EXTENSIONS
#pragma bss_seg(	".system_bss")
#pragma data_seg(	".system_data")
#pragma const_seg(	".system_const")
#pragma code_seg(	".system_code")
#endif

#define TASK_IDLE_DEBUG_ENABLE

#ifdef TASK_IDLE_DEBUG_ENABLE
#define task_idle_printf log_printf
#else
#define task_idle_printf(...)
#endif// TASK_IDLE_DEBUG_ENABLE

extern u32 os_time_get(void);
extern u8 usb_slave_is_online(void);
extern u8 get_battery_charge_flag(void);



u8 test_read(void)
{
    u8 f_buf[512]={'\0'};
    s32 err, f_size, i=0, j=0, k=0;
    FILE_OPERATE *fop_api;


    fop_api = file_operate_creat();

    if (fop_api == NULL)
    {
        printf("creat fop_api err...\n");
        return false;
    }

    file_operate_set_dev(fop_api, (u32)sd0);//only use sd0

    err = file_api_creat(fop_api->fop_file, (void *)fop_api->fop_info->dev, 0);

    if (err == false) {
        printf("creat file_api err...\n");
        return false;
    }

    err = fs_get_file_bypath(&(fop_api->fop_file->fs_hdl),&(fop_api->fop_file->file_hdl), (void *)"/test.bin");
    printf("err = 0x%x \n", err);

    if(err == 0)
    {
        if(fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FILE_SIZE, &f_size))
        {
            printf("read f_size err...\n");
            return false;
        }

        if(f_size > 512)
            f_size = 512;

        fs_read(&(fop_api->fop_file->file_hdl), f_buf, f_size);

        printf("f_size:%d \n", f_size);
        put_buf(f_buf, f_size);

        fs_close(&(fop_api->fop_file->file_hdl));
    }
    else
    {
        err = fs_open(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), (char *)"/test.bin", FA_CREATE_NEW | FA_WRITE);
        if (!err)
        {
            printf("--create test.bin success...\n");

            fs_write(&(fop_api->fop_file->file_hdl), f_buf, sizeof(f_buf));
            fs_sync(&(fop_api->fop_file->file_hdl));

            fs_delete(&(fop_api->fop_file->file_hdl));
        }
        else
        {
            printf("--create test.bin fail...err= %d\n",err);
        }

        fs_close(&(fop_api->fop_file->file_hdl));
    }

    return true;
}






static tbool task_idle_skip_check(void **priv)
{
    task_idle_printf("task_idle_skip_check !!\n");
    int error = MSG_NO_ERROR;
    int msg = NO_MSG;

    if(*priv ==IDLE_POWER_UP)
    {
        if(USB_IN_STATE()== 1)
            return true;

        while (1)
        {
            error = task_get_msg(0, 1, &msg);

            if (os_time_get() <300)//3S
            {
                 if (task_common_msg_deal(*priv, msg) == false) //*priv 为了PC接入后开机时不立即跳转。  检测SD存在。
                 break;
            }
            else
                break;
        }
    }

    return true;
}


static void *task_idle_init(void *priv)
{
    /* dac_channel_on(MUSIC_CHANNEL, 0); */
    task_idle_printf("task_idle_init !!\n");

    if (priv == IDLE_POWER_OFF)
    {
        tone_play(TONE_POWER_OFF, 0);
    }
    else if (priv == IDLE_POWER_UP)
    {
        if(USB_IN_STATE() == 0)  //不是USB 线开机
        {
            fat_init();
            fat_mem_init(NULL, 0);  //使用默认的RAM， overlay task
            tone_play(TONE_POWER_ON, 0);
            led_fre_set(C_BLED_SLOW_MODE);
        }
        #if (USB_PC_EN)
        else if(usb_slave_is_online()) //USB 接入插座or电脑开机进入PC模式
        {
            task_switch(TASK_ID_PC, NULL);
        }
        #endif
    }
    else if(priv == IDLE_SD_ERROR)
    {
        tone_play(TONE_SD_ERROR, 0);
    }
    else
    {
        if(USB_IN_STATE() == 1)
        {
            test_read();

            dev_offline_unmount(sd0);

            SWITCH_HIGH();
        }


        task_idle_printf("*priv:%x !!\n", priv);
    }

    return priv;
}

static void task_idle_exit(void **hdl)
{
    task_idle_printf("task_idle_exit !!\n");
    task_clear_all_message();
}


static void task_idle_deal(void *hdl)
{
    int error = MSG_NO_ERROR;
    int msg = NO_MSG;
    task_idle_printf("task_idle_deal !!\n");
    u32 i = 0;
    u8 flag = 0;

    while (1) {

        error = task_get_msg(0, 1, &msg);

        if (task_common_msg_deal(hdl, msg) == false)
        {
           // return ;
        }

        if (NO_MSG == msg) {
            continue;
        }

        task_idle_printf("idle msg = %x\n", msg);
        switch (msg) {
        case MSG_HALF_SECOND:
            /* task_idle_printf("-I_H-");
            if ((os_time_get() > 800) && (os_time_get() < 1000)) {	//5~8s
                task_time_out, run default task
                task_switch(TASK_ID_BT, NULL);
                return;
            }
            */
            break;
        case MSG_ONE_SECOND:
        case SYS_EVENT_DEC_END:
            /* task_idle_printf("\n---------------sbc notice SYS_EVENT_DEC_END\n"); */
            /* sbc_notice_stop(sbc_hdl); */
            /* sbc_notice_play(i); */
            /* i++; */
            break;

        case SYS_EVENT_PLAY_SEL_END:
            task_idle_printf("SYS_EVENT_PLAY_TONE_END\n");

            if((hdl == IDLE_POWER_OFF) || (tone_var.idx == TONE_SD_ERROR)) //关机和卡错误
            {
                task_idle_printf("idle enter soft_poweroff\n");
                #if RTC_CLK_EN
                set_lowpower_keep_32K_osc_flag(1);
                #endif
                SPEAKER_DECODER_LOW();
                sound_drv->off(NULL); ///关闭 DAC 模块
                delay(500000);delay(500000); ///根据实际延时
                delay(500000);delay(500000);
                delay(500000);delay(500000);
                delay(500000);delay(500000); ///根据实际延时
                delay(500000);delay(500000);
                delay(500000);delay(500000);
                if(USB_IN_STATE() == 0)
                    enter_sys_soft_poweroff();
            }
            else if(hdl == IDLE_POWER_UP)
            {
                #if USB_OTHER_PC
                if (USB_IN_STATE() == 1)
                {

                }
                else
                #endif
                #if (USB_PC_EN)
                else if(usb_slave_is_online()) //USB 接入插座or电脑开机进入PC模式
                {
                    task_switch(TASK_ID_PC, NULL);
                }
                else
                #endif
                if(dev_get_fisrt(MUSIC_DEV_TYPE, DEV_ONLINE) == NULL)
                {
                    tone_play(TONE_SD_ERROR, 0);
                    break;
                }
                else
                    task_switch(TASK_ID_OID, NULL);//提示音结束进入OID

                return ;
            }
            break;

        case MSG_POWER_OFF:
            task_idle_printf("idle power off\n");
            #if RTC_CLK_EN
            set_lowpower_keep_32K_osc_flag(1);
            #endif
            enter_sys_soft_poweroff();
            break;

        case MSG_TURN_TO_IDLE:
            if(USB_IN_STATE() == 1)
            {
                SWITCH_HIGH();
            }
            break;

        default:
            break;
        }
    }
}

const TASK_APP task_idle_info = {
    /* .name 		= TASK_APP_IDLE, */
    .skip_check = task_idle_skip_check,
    .init 		= task_idle_init,
    .exit 		= task_idle_exit,
    .task 		= task_idle_deal,
    .key 		= &task_idle_key,
};

